const express = require('express')
const router = express.Router()
const { db } = require('./routerSetupFirebase');

let ref = db.ref('profile')

router.get('/detail', async (req, res) => {
  try {
    await ref.on('value', (snapshot) => {
      res.send(snapshot)
    })
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post('/login', async (req, res) => {
  try {
    await firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
    await firebase.auth().onAuthStateChanged((user) => {
      res.send(user)
    })
  } catch (e) {
    res.status(400).send(e);
  }
});


module.exports = router;
