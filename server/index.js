const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const routerAuth = require('./router/routerAuth');

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use('/auth', routerAuth);


module.exports = { app };
