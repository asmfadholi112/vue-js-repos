## Demo

Check link below: 
[website](https://vue-project-repo.herokuapp.com/#/)


## Quick start

Quick start options:

- Run `npm install`
- Run `npm run serve` to start a local development server


## Documentation
The documentation for Vue Now UI Kit is hosted at our [website](https://demos.creative-tim.com/vue-now-ui-kit/documentation/).


## File Structure

Within the download you'll find the following directories and files:

```
Vue Now Ui Kit
|-- src
        |-- App.vue
        |-- main.js
        |-- router.js
        |-- assets
        |   |-- fonts
        |   |-- scss
        |-- components
        |-- directives
        |-- layout
        |-- pages
        |-- plugins
```

## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

<img src="src/assets/github/chrome.png" width="64" height="64"> <img src="src/assets/github/firefox.png" width="64" height="64"> <img src="src/assets/github/edge.png" width="64" height="64"> <img src="src/assets/github/safari.png" width="64" height="64"> <img src="src/assets/github/opera.png" width="64" height="64">


## Resources
- Demo: https://demos.creative-tim.com/vue-now-ui-kit
- Download Page: https://www.creative-tim.com/product/vue-now-ui-kit
- Documentation: https://demos.creative-tim.com/vue-now-ui-kit/documentation/
- License Agreement: https://www.creative-tim.com/license
- Support: https://www.creative-tim.com/contact-us
- Issues: [Github Issues Page](https://github.com/creativetimofficial/vue-now-ui-kit/issues)
- [Vue Now UI Dashboard PRO - For Dashboard development](https://www.creative-tim.com/product/vue-now-ui-dashboard-pro?ref=github-vue-nud-free)

## Reporting Issues
We use GitHub Issues as the official bug tracker for Vue Now UI Kit. Here are some advices for our users that want to report an issue:

1. Make sure that you are using the latest version of the Vue Now UI Kit. Check the CHANGELOG from your ui kit on our [website](https://www.creative-tim.com/).
2. Providing us reproducible steps for the issue will shorten the time it takes for it to be fixed.
3. Some issues may be browser specific, so specifying in what browser you encountered the issue might help.

## Technical Support or Questions

If you have questions or need help integrating the product please [contact us](https://www.creative-tim.com/contact-us) instead of opening an issue.

## Licensing

- Copyright 2018 Creative Tim (https://www.creative-tim.com)
- Licensed under MIT (https://github.com/creativetimofficial/vue-now-ui-kit/blob/master/LICENSE.md)

## Useful Links

More products from Creative Tim: <https://www.creative-tim.com/products>

Tutorials: <https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w>

Freebies: <https://www.creative-tim.com/products>

Affiliate Program (earn money): <https://www.creative-tim.com/affiliates/new>

Social Media:

Twitter: <https://twitter.com/CreativeTim>

Facebook: <https://www.facebook.com/CreativeTim>

Dribbble: <https://dribbble.com/creativetim>

Google+: <https://plus.google.com/+CreativetimPage>

Instagram: <https://instagram.com/creativetimofficial>
