import router from '@/routers'
import firebase from 'firebase'
import { db } from '@/services/firebaseSetup'

export default function auth ({ next }) {
 firebase.auth().onAuthStateChanged((user) => {
    if (!user) {
      return router.push('/')
    } else {
      return next()
    }
  })
}
