import Login from '@/pages/Login.vue';
import router from '@/routers'
import firebase from 'firebase'
import { db } from '@/services/firebaseSetup'


export default [
    {
      path: '/',
      name: 'login',
      components: { default: Login },
      beforeEnter: (to, from, next) => {
        firebase.auth().onAuthStateChanged((user) => {
          if (!user) {
            next()
          } else {
            router.push('/profile')
          }
        })
      }
    },
     
  ];
  