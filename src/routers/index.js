import Vue from 'vue';
import Router from 'vue-router';
import routerLogin from './modules/routerLogin'
import routerDashboard from './modules/routerDashboard'

Vue.use(Router);

// register modules here
const moduleRoutes = [
  routerLogin,
  routerDashboard,
]

const baseRoutes = [
  {
    path: '*',
    redirect: '/'
  }
]

const routes = baseRoutes.concat(...moduleRoutes)

const router = new Router({
  linkExactActiveClass: 'active',
  mode: 'history',
  routes,
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
})

// validate route by middleware
function nextFactory (context, middleware, index) {
  const subsequentMiddleware = middleware[index]

  if (!subsequentMiddleware) return context.next

  return () => {
    const nextMiddleware = nextFactory(context, middleware, index + 1)
    subsequentMiddleware({ ...context, next: nextMiddleware })
  }
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware]

    const context = {
      from,
      next,
      router,
      to
    }

    const nextMiddleware = nextFactory(context, middleware, 1)
    return middleware[0]({ ...context, next: nextMiddleware })
  } else {
    next()
  }
})

export default router
