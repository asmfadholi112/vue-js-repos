import { db } from '@/services/firebaseSetup'

let profile = db.ref('profile')

export default {
    methods: {
        async getProfile(req, res) {
            try {
                await profile.on('value', (val) => {
                    res.result = val.val()[Object.keys(val.val())[0]]
                })
            } catch (e) {
                res = e;
            }
        },
    }
}