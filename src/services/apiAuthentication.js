import firebase from 'firebase'
import { db } from '@/services/firebaseSetup'
import router from '@/routers'

export default {
    methods: {
        async loginAuthentication(req, res) {
            try {
                await firebase.auth().signInWithEmailAndPassword(req.email, req.password)
                router.replace('/profile')
            } catch (e) {
                res = e;
            }
        },

        async logoutAuthentication() {
            try {
                await firebase.auth().signOut()
            } catch (e) {
                res = e;
            }
        }
    }
}